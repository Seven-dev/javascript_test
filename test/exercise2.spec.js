const {expect, assert} = require('chai');
const sinon = require('sinon');
const exercise2 = require('../src/exercise2');
const {TestLogger} = require('./utils');

describe('#exercise2()', function() {
    let logger = null;
    let stub = null;
    let countries = [
        {
        country: "US",
        languages: [ "en" ]
        },
        {
        country: "BE",
        languages: [ "nl", "fr", "de" ]
        },
        {
        country: "NL",
        languages: [ "nl", "fy" ]
        },
        {
        country: "DE",
        languages: [ "de" ]
        },
        {
        country: "ES",
        languages: [ "es" ]
        }
    ];

    before(function() {
        // Replace console.log with a fake logger so it can be easier to analyse
        logger = new TestLogger();
        stub = sinon.stub(console, "log").callsFake(logger.log.bind(logger));
    });
    afterEach(function() {
        logger.reset();
    });
    after(function () {
        console.log.restore(); // Unwraps the spy
    });
    context('function structure and input', function() {
        it('should throw an error if there\'s no argument', function() {
            expect(() => exercise2()).to.throw('the argument countries is not an Array');
        });

        it('should throw an error when the countries argument is not an Array', function() {
            expect(() => exercise2(3)).to.throw('the argument countries is not an Array');
        });

        it('should throw an error when the countries argument\'s items are not objects', function() {
            expect(() => exercise2([3])).to.throw('values inside the argument countries are not objects');
        });

        it('should throw an error when the countries argument\'s items have the wrong parameters', function() {
            expect(() => exercise2([{}])).to.throw('values inside the argument countries do not contain one of the following keys: country, languages');
        });
    });

    context('function logic', function() {
        it('should have the same output for the same countries', function() {
            let results = exercise2(countries);
            expect(JSON.stringify(results)).to.equal(JSON.stringify({
                numberOfCountriesInWorld: 5,
                countryMostLanguagesAndGerman: { country: 'BE', languages: [ 'nl', 'fr', 'de' ] },
                numberAllOfficialLanguages: 6,
                countryMostLanguages: { country: 'BE', languages: [ 'nl', 'fr', 'de' ] },
                mostCommonOfficialLanguages: [ { count: 2, language: 'nl' }, { count: 2, language: 'de' } ]
            }));
        });
    });
});