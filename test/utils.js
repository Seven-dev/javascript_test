/**
 * Class that replaces console.log in the tests
 */
class TestLogger {
    logs = [];
    log(text){
        this.logs.push(text);
    }
    reset(){
        this.logs = [];
    }
}

module.exports = {
    TestLogger
};