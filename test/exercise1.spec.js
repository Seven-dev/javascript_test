const {expect, assert} = require('chai');
const sinon = require('sinon');
const exercise1 = require('../src/exercise1');
const {TestLogger} = require('./utils');

describe('#exercise1()', function() {
    let logger = null;
    let stub = null;
    before(function() {
        // Replace console.log with a fake logger so it can be easier to analyse
        logger = new TestLogger();
        stub = sinon.stub(console, "log").callsFake(logger.log.bind(logger));
    });
    afterEach(function() {
        logger.reset();
    });
    after(function () {
        console.log.restore(); // Unwraps the spy
    });
    context('function structure and input', function() {
        it('should always return undefined - no input', function() {
            let result1 = exercise1();
            expect(result1).to.equal(undefined);
        });

        it('should always return undefined - input', function() {
            let result2 = exercise1(100);
            expect(result2).to.equal(undefined);
        });

        it('should throw an error when the limit input is a string', function() {
            expect(() => exercise1('some string')).to.throw('the argument limit is not a number');
        });
    });

    context('function logic', function() {
        it('should create 100 logs by default', function() {
            exercise1();
            expect(logger.logs.length).to.equal(100);
        });

        it('should print the word "Visual Nuts" only when the number is divisible by 3 and 5', function() {
            let limit = 100;
            exercise1(limit);
            for(let logIndex = 1; logIndex < logger.logs.length; logIndex++){
                if((logIndex % 3 === 0) && (logIndex % 5 === 0)){
                    expect(logger.logs[logIndex-1]).to.equal('Visual Nuts');
                }else{
                    expect(logger.logs[logIndex-1]).to.not.equal('Visual Nuts');
                }
            }
        });

        it('should print the word "Visual" when the number is divisible by 3 (but not by 5)', function() {
            let limit = 100;
            exercise1(limit);
            for(let logIndex = 1; logIndex < logger.logs.length; logIndex++){
                if((logIndex % 3 === 0) && (logIndex % 5 !== 0)){
                    expect(logger.logs[logIndex-1]).to.equal('Visual');
                }
            }
        });

        it('should print the word "Nuts" when the number is divisible by 5 (but not by 3)', function() {
            let limit = 100;
            exercise1(limit);
            for(let logIndex = 1; logIndex < logger.logs.length; logIndex++){
                if((logIndex % 5 === 0) && (logIndex % 3 !== 0)){
                    expect(logger.logs[logIndex-1]).to.equal('Nuts');
                }
            }
        });
    });
});