# Javascript Test

This is my answer to a javascript test.

## Installation

```bash
# Use version 12 of node
# If you use nvm try this:
nvm use 12

# Install dependencies
npm install

# Run
npm start
```

## Tests
Execute the command below to run the tests.
```bash
npm test
```

## Documentation  
Execute the command below to generate documentation for the project.  
You can find it inside the folder `docs` by opening `index.html`.  
```bash
npm run docs
```