// Exercise 1

/**
 * Prints numbers from 1 to 100, with these exceptions:
 *   - When the number is divisible by 3, do not print the number, but print the word 'Visual'. 
 *   - When the number is divisible by 5, do not print the number, but print 'Nuts'. 
 *   - And for all numbers divisible by both (eg: the number 15) the same, but print 'Visual Nuts'.
 * @param {int} limit Maximum number to print
 */
function exercise1(limit = 100){
    if(isNaN(limit)){
        throw 'the argument limit is not a number';
    }
    // Print integer numbers from 1 to 100
    for(let count = 1; count < limit+1; count++){
        if(count % 5 == 0 && count % 3 == 0){
            console.log('Visual Nuts');
        }else if(count % 3 == 0){
            console.log('Visual');
        }else if(count % 5 == 0){
            console.log('Nuts');
        }else{
            console.log(count);
        }
    }
}

module.exports = exercise1;