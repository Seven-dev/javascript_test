const exercise1 = require('./exercise1');
const exercise2 = require('./exercise2');

let exampleCountries = [
    {
    country: "US",
    languages: [ "en" ]
    },
    {
    country: "BE",
    languages: [ "nl", "fr", "de" ]
    },
    {
    country: "NL",
    languages: [ "nl", "fy" ]
    },
    {
    country: "DE",
    languages: [ "de" ]
    },
    {
    country: "ES",
    languages: [ "es" ]
    }
];

// Exercise 1
console.log('Exercise 1:');
exercise1();

console.log('\n-------------------------\n');

// Exercise 2
console.log('Exercise 2:');
exercise2(exampleCountries);