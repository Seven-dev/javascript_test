
/**
 * @param {Array} countries 
 * @returns The number of countries inside the input variable.
 */
function getNumberOfCountriesInWorld(countries){
    return countries.length;
}

/**
 * @param {Array} countries 
 * @returns The country with the most official languages, where they officially speak German.
 */
function getCountryMostLanguagesAndGerman(countries){
    let countriesGerman = countries.filter(country => country.languages.includes('de'));
    // Sort countries by number of languages and return the one with the highest
    return countriesGerman.sort((a, b) => b.languages.length -a.languages.length)[0];
}

/**
 * @param {Array} countries 
 * @returns The count of all the official languages spoken in the listed countries.
 */
function countAllOfficialLanguages(countries){
    return [...new Set(countries.map(country => country.languages).flat())].length;
}

/**
 * 
 * @param {Array} countries 
 * @returns The country with the highest number of official languages.
 */
function getCountryMostLanguages(countries){
    return countries.sort((a, b) => b.languages.length -a.languages.length)[0];
}

/**
 * @param {Array} countries 
 * @returns The most common official language(s), of all countries.
 */
function getMostCommonLanguagesInWorld(countries){
    // Create frequency distribution of all the languages
    let languageFrequencyDistribution = countries.reduce((acc, item) => {
        for(let language of item.languages){
            acc[language] = (acc[language] || 0) + 1;
        }
        return acc;
    }, {});
    
    let languageFrequencyDistributionArray = Object.keys(languageFrequencyDistribution).map(k => ({count: languageFrequencyDistribution[k], language: k}));
    let max = 0;
    let mostCommonLanguages = [];
    for(let language of languageFrequencyDistributionArray){
        if(language.count > max){
            mostCommonLanguages = [language];
            max = language.count;
        }else if(language.count == max){
            mostCommonLanguages.push(language);
        }
    }
    return mostCommonLanguages;
}

/**
 * Simple validation for the countries input.
 * @param {Array} countries 
 */
function countriesValidations(countries){
    if(!(countries instanceof Array)){
        throw 'the argument countries is not an Array';
    }
    countries.forEach(country => {
        if(country.constructor !== Object){
            throw 'values inside the argument countries are not objects';
        }
        if(!(Object.keys(country).includes('country') && Object.keys(country).includes('languages'))){
            throw 'values inside the argument countries do not contain one of the following keys: country, languages';
        }
    });
}

// Exercise 2
/**
 * An exercise based on the official languages spoken by countries.
 * @param {Array} countries 
 * @returns An object with all the parameters asked in the exercise.
 */
function exercise2(countries){
    countriesValidations(countries);

    let numberOfCountriesInWorld = getNumberOfCountriesInWorld(countries);
    let countryMostLanguagesAndGerman = getCountryMostLanguagesAndGerman(countries);
    let numberAllOfficialLanguages = countAllOfficialLanguages(countries);
    let countryMostLanguages = getCountryMostLanguages(countries);
    let mostCommonOfficialLanguages = getMostCommonLanguagesInWorld(countries);

    console.log('Number of countries in the world:', numberOfCountriesInWorld);
    console.log('Country with the most official languages, where they officially speak German:', countryMostLanguagesAndGerman);
    console.log('Number of all the official languages spoken:', numberAllOfficialLanguages);
    console.log('Country with the highest number of official languages:', countryMostLanguages);
    console.log('Most common official languages:', mostCommonOfficialLanguages);
    return {
        numberOfCountriesInWorld,
        countryMostLanguagesAndGerman,
        numberAllOfficialLanguages,
        countryMostLanguages,
        mostCommonOfficialLanguages
    };
}

module.exports = exercise2;